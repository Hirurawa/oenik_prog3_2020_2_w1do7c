﻿// <copyright file="IAnimalRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Repository
{
    using Shelter.Data.Models;

    /// <summary>
    /// Animal repository interface.
    /// </summary>
    public interface IAnimalRepository : IRepository<Animal>
    {
        /// <summary>
        /// Adds one animal.
        /// </summary>
        /// <param name="animal">The animal you wish to add.</param>
        /// <returns>The ID of the newly added animal.</returns>
        public int Add(Animal animal);

        /// <summary>
        /// Removes the animal specified in the argument.
        /// </summary>
        /// <param name="id">ID of the animal.</param>
        /// <returns>wether the removal was a success or not.</returns>
        public bool Remove(int id);

        /// <summary>
        /// Changes the age of the animal specified in the argument.
        /// </summary>
        /// <param name="id">The ID of the animal.</param>
        /// <param name="newAge">The new ade of the animal.</param>
        void ChangeAge(int id, int newAge);
    }
}
