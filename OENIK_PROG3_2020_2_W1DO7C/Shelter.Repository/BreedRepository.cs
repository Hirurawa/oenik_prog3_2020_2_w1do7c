﻿// <copyright file="BreedRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Shelter.Data.Models;

    /// <summary>
    /// Breed repository.
    /// </summary>
    public class BreedRepository : Repository<Breed>, IBreedRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BreedRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        public BreedRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Adds a new breed.
        /// </summary>
        /// <param name="name">Name of the new breed.</param>
        /// <returns>The ID of the newly added breed.</returns>
        public int Add(string name)
        {
            Breed breed = new Breed
            {
                Name = name,
            };
            this.ctx.Set<Breed>().Add(breed);
            this.ctx.SaveChanges();
            return breed.Id;
        }

        /// <summary>
        /// Gets one breed specified in the argument.
        /// </summary>
        /// <param name="id">ID of the breed.</param>
        /// <returns>The searched breed.</returns>
        public override Breed GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}
