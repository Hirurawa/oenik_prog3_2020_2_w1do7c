﻿// <copyright file="IBreedRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Repository
{
    using Shelter.Data.Models;

    /// <summary>
    /// Breed repository interface.
    /// </summary>
    public interface IBreedRepository : IRepository<Breed>
    {
        /// <summary>
        /// Adds a new breed.
        /// </summary>
        /// <param name="name">The name of the new breed.</param>
        /// <returns>The ID of the newly added breed.</returns>
        public int Add(string name);
    }
}
