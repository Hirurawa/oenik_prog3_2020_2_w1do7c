﻿// <copyright file="Breed.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Breed class. This class is used to store the Breed entities.
    /// </summary>
    [Table("Breed")]
    public class Breed
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Breed"/> class.
        /// </summary>
        public Breed()
        {
            this.Animals = new HashSet<Animal>();
        }

        /// <summary>
        /// Gets or sets unique identificator of the breed.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the breed.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets collection of animal entities in the breed.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Animal> Animals { get; set; }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns>A formatted string which contains all the relevant information about the breed.</returns>
        public override string ToString()
        {
            return $"Name: {this.Name}";
        }
    }
}
