﻿// <copyright file="ShelterContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Data.Models
{
    using System;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ShelterContext partial class. Derives from DbContext.
    /// </summary>
    public partial class ShelterContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShelterContext"/> class.
        /// </summary>
        public ShelterContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets dbSet colelction of the animals.
        /// </summary>
        public virtual DbSet<Animal> Animals { get; set; }

        /// <summary>
        /// Gets or sets dbSet collection of the breeds.
        /// </summary>
        public virtual DbSet<Breed> Breeds { get; set; }

        /// <summary>
        /// Generated method.
        /// </summary>
        /// <param name="optionsBuilder">.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Shelter.mdf;Integrated Security=True;MultipleActiveResultSets = true");
            }
        }

        /// <summary>
        /// Generated method.
        /// </summary>
        /// <param name="modelBuilder">.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException(nameof(modelBuilder));
            }

            modelBuilder.Entity<Animal>(entity =>
            {
                entity
                .HasOne(animal => animal.Breed)
                .WithMany(breed => breed.Animals)
                .HasForeignKey(animal => animal.BreedId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            });

            Animal a1 = new Animal()
            {
                Id = 1,
                Name = "Szimat",
                MF = Animal.Sex.Female,
                Age = 11,
                BreedId = 1,
            };

            Animal a2 = new Animal()
            {
                Id = 2,
                Name = "Pöpec",
                MF = Animal.Sex.Male,
                Age = 2,
                BreedId = 1,
            };

            Animal a3 = new Animal()
            {
                Id = 3,
                Name = "Muki",
                MF = Animal.Sex.Female,
                Age = 3,
                BreedId = 2,
            };

            Animal a4 = new Animal()
            {
                Id = 4,
                Name = "Lüke",
                MF = Animal.Sex.Male,
                Age = 10,
                BreedId = 3,
            };

            Animal a5 = new Animal()
            {
                Id = 5,
                Name = "Pracli",
                MF = Animal.Sex.Female,
                Age = 7,
                BreedId = 3,
            };

            Breed b1 = new Breed()
            {
                Id = 1,
                Name = "Mixed",
            };

            Breed b2 = new Breed()
            {
                Id = 2,
                Name = "Labrador",
            };

            Breed b3 = new Breed()
            {
                Id = 3,
                Name = "Pug",
            };

            modelBuilder.Entity<Breed>().HasData(b1, b2, b3);
            modelBuilder.Entity<Animal>().HasData(a1, a2, a3, a4, a5);
        }
    }
}
