﻿// <copyright file="Animal.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Data.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Animal class. This class is used to store the Animal entities.
    /// </summary>
    [Table("Animal")]
    public class Animal
    {
        /// <summary>
        /// Sex of the Animal. It can only be male or female.
        /// </summary>
        public enum Sex
        {
            /// <summary>
            /// Male animal.
            /// </summary>
            Male,

            /// <summary>
            /// Female animal.
            /// </summary>
            Female,
        }

        /// <summary>
        /// Gets or sets unique identificator of the animal.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name of the animal.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets sex of the animal.
        /// </summary>
        [Required]
        public Sex MF { get; set; }

        /// <summary>
        /// Gets or sets age of the animal.
        /// </summary>
        [Required]
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets id of the breed, where the animal belongs.
        /// </summary>
        [ForeignKey(nameof(Breed))]
        public int BreedId { get; set; }

        /// <summary>
        /// Gets or sets the breed of the animal.
        /// </summary>
        [NotMapped]
        public virtual Breed Breed { get; set; }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns>A formatted string which contains all the relevant information about the animal.</returns>
        public override string ToString()
        {
            return $"[{this.Id}] Name: {this.Name}, Age: {this.Age}, Sex: {this.MF}, BreedId: {this.BreedId}, Breed: {this.Breed.Name}";
        }
    }
}
