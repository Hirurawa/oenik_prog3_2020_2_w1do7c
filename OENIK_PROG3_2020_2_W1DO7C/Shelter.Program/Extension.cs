﻿namespace Shelter.Program
{
    using System;
    using System.Collections.Generic;

    public static class Extension
    {
        public static void ToConsole<T>(this IEnumerable<T> input, string header)
        {
            Console.WriteLine($"*+*+*+*+*+ {header} +*+*+*+*+*");
            foreach (var item in input)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine($"*+*+*+*+*+ {header} +*+*+*+*+*");
            Console.ReadLine();
        }
    }
}
