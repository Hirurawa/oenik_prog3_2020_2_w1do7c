﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Program
{
    using System;
    using ConsoleTools;
    using Shelter.Data.Models;
    using Shelter.Logic;
    using Shelter.Repository;

    /// <summary>
    /// The entry point of the program.
    /// </summary>
    internal class Program
    {
        private static readonly ShelterContext Ctx = new ShelterContext();

        private static readonly AnimalRepository AnimalRepo = new AnimalRepository(Ctx);

        private static readonly BreedRepository BreedRepo = new BreedRepository(Ctx);

        private static readonly ShelterLogic Logic = new ShelterLogic(AnimalRepo, BreedRepo);

        /// <summary>
        /// Program starts here.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        private static void Main(string[] args)
        {
            var menu = new ConsoleMenu(args, level: 0)
                .Add("Average ages by breed", () => Logic.GetBreedAverageAge().ToConsole("Average ages by breed"))
                .Add("Add new animal", () =>
                {
                    Console.Write("Animal's name: ");
                    string name = Console.ReadLine();
                    Console.Write("The animal's age: ");
                    int age = 0;
                    _ = int.TryParse(Console.ReadLine(), out age);
                    Console.Write("Animal's breed: ");
                    int breed = BreedSelect();
                    Console.Write("Animal's sex: (0-Male/1-Female) ");
                    int sex = 0;
                    _ = int.TryParse(Console.ReadLine(), out sex);
                    Animal newAnimal = new Animal()
                    {
                        Name = name,
                        Age = age,
                        BreedId = breed,
                        MF = (Animal.Sex)sex,
                    };
                    try
                    {
                        Logic.AddAnimal(newAnimal);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error while parsing user input! Could not add new animal.");
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                        throw;
                    }
                })
                .Add("Add new breed", () =>
                {
                    Console.Write("Breed's name: ");
                    string name = Console.ReadLine();
                    Logic.AddBreed(name);
                })
                .Add("Remove animal", () =>
                {
                    Console.Write("ID of the animal, you want to remove: ");
                    _ = int.TryParse(Console.ReadLine(), out int animalId);
                    Logic.RemoveAnimal(animalId);
                })
                .Add("List all animals", () => Logic.GetAllAnimals().ToConsole("All animals"))
                .Add("List all breeds", () => Logic.GetAllBreeds().ToConsole("All breeds"))
                .Add("List all animals by breed", () =>
                {
                    int b = BreedSelect();
                    if (b != -1)
                    {
                        Logic.GetAnimalsByBreed(b).ToConsole("All");
                    }
                })
                .Add("Oldest animal", () =>
                {
                    Console.WriteLine(Logic.GetOldestAnimal().ToString());
                    Console.ReadLine();
                })
                .Add("Close", ConsoleMenu.Close);
            menu.Show();
        }

        private static int BreedSelect()
        {
            Console.WriteLine("Loading breed selector. . .");
            int b = -1;
            var breedSelector = new ConsoleMenu();
            foreach (var item in Logic.GetAllBreeds())
            {
                breedSelector.Add(item.Name, () =>
                {
                    b = item.Id;
                    breedSelector.CloseMenu();
                });
            }

            breedSelector.Add("Back", ConsoleMenu.Close);
            breedSelector.Configure(config =>
            {
                config.WriteHeaderAction = () => { };
                config.EnableWriteTitle = true;
                config.Title = "Select a breed!";
            });
            breedSelector.Show();
            return b;
        }
    }
}
