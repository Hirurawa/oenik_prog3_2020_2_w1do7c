﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using Shelter.Data.Models;
    using Shelter.Logic;
    using Shelter.Repository;

    /// <summary>
    /// Tests.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private Mock<IAnimalRepository> animalRepo;
        private Mock<IBreedRepository> breedRepo;
        private List<Averages> expectedAverages;

        /// <summary>
        /// CRUD: GetAll.
        /// </summary>
        [Test]
        public void TestGetByBreed()
        {
            // ARRANGE
            Mock<IAnimalRepository> mockedRepo = new Mock<IAnimalRepository>(MockBehavior.Loose); // default
            List<Animal> animals = new List<Animal>()
            {
                new Animal() { Name = "Animal1", BreedId = 1 },
                new Animal() { Name = "Animal2", BreedId = 2 },
                new Animal() { Name = "Animal3", BreedId = 2 },
                new Animal() { Name = "Animal4", BreedId = 3 },
            };

            List<Animal> expectedRows = new List<Animal>() { animals[1], animals[2] };

            mockedRepo.Setup(repo => repo.GetAll()).Returns(animals.AsQueryable());
            ShelterLogic logic = new ShelterLogic(mockedRepo.Object);

            // ACT
            var result = logic.GetAnimalsByBreed(2);

            // ASSERT
            Assert.That(result.Count, Is.EqualTo(expectedRows.Count));
            Assert.That(result, Is.EquivalentTo(expectedRows));

            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// CRUD: Insert.
        /// </summary>
        [Test]
        public void TestAddBreed()
        {
            Mock<IAnimalRepository> animalRepo = new Mock<IAnimalRepository>();
            Mock<IBreedRepository> breedRepo = new Mock<IBreedRepository>();
            breedRepo.Setup(repo => repo.Add(It.IsAny<string>())).Returns(42);
            ShelterLogic logic = new ShelterLogic(animalRepo.Object, breedRepo.Object);

            int idNumber = logic.AddBreed("Pug");

            Assert.That(idNumber, Is.EqualTo(42));
            breedRepo.Verify(repo => repo.Add("Pug"), Times.Once);
        }

        /// <summary>
        /// CRUD: GetOne.
        /// </summary>
        [Test]
        public void TestGetOneAnimal()
        {
            Mock<IAnimalRepository> animalRepo = new Mock<IAnimalRepository>();
            Mock<IBreedRepository> breedRepo = new Mock<IBreedRepository>();
            animalRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(new Animal() { Name = "name", Id = 0 });

            ShelterLogic logic = new ShelterLogic(animalRepo.Object, breedRepo.Object);

            Animal b = logic.GetOneAnimal(0);

            Assert.That(b.Name, Is.EqualTo("name"));
            animalRepo.Verify(repo => repo.GetOne(0), Times.Once);
        }

        /// <summary>
        /// CRUD: Remove.
        /// </summary>
        [Test]
        public void TestRemoveAnimal()
        {
            Mock<IAnimalRepository> animalRepo = new Mock<IAnimalRepository>();
            animalRepo.Setup(repo => repo.Remove(It.IsAny<int>())).Returns(true);
            ShelterLogic logic = new ShelterLogic(animalRepo.Object);

            bool result = logic.RemoveAnimal(0);

            Assert.That(result, Is.EqualTo(true));
            animalRepo.Verify(repo => repo.Remove(0), Times.Once);
        }

        /// <summary>
        /// CRUD: Update.
        /// </summary>
        [Test]
        public void TestChangeAnimalAge()
        {
            Mock<IAnimalRepository> animalRepo = new Mock<IAnimalRepository>();
            List<Animal> animals = new List<Animal>()
            {
                new Animal() { Id = 0, Age = 1 },
                new Animal() { Id = 1, Age = 2 },
                new Animal() { Id = 2, Age = 3 },
            };
            animalRepo.Setup(repo => repo.GetAll()).Returns(animals.AsQueryable());
            animalRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(new Animal() { Age = 10, Id = 1 });
            ShelterLogic logic = new ShelterLogic(animalRepo.Object);

            logic.ChangeAnimalAge(1, 10);

            Assert.That(logic.GetOneAnimal(1).Age, Is.EqualTo(10));
            animalRepo.Verify(repo => repo.GetOne(1), Times.Once);
        }

        /// <summary>
        /// non-CRUD test for the averages.
        /// </summary>
        [Test]
        public void TestGetAverages()
        {
            var logic = this.CreateLogicWithMocks();
            var actualAverages = logic.GetBreedAverageAge();

            Assert.That(actualAverages, Is.EquivalentTo(this.expectedAverages));
            this.animalRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.breedRepo.Verify(repo => repo.GetAll(), Times.Never);
        }

        /// <summary>
        /// non-CRUD test for the oldest animal.
        /// </summary>
        [Test]
        public void TestOldestAnimal()
        {
            var logic = this.CreateLogicWithMocks();
            var oldestAnimal = logic.GetOldestAnimal();

            Assert.That(oldestAnimal.Age, Is.EqualTo(6));
        }

        private ShelterLogic CreateLogicWithMocks()
        {
            this.animalRepo = new Mock<IAnimalRepository>();
            this.breedRepo = new Mock<IBreedRepository>();

            Breed mixed = new Breed() { Id = 1, Name = "Mixed" };
            Breed vizsla = new Breed() { Id = 2, Name = "Vizsla" };
            List<Breed> breeds = new List<Breed>() { mixed, vizsla };
            List<Animal> animals = new List<Animal>()
            {
                new Animal() { Breed = mixed, BreedId = mixed.Id, Age = 6 },
                new Animal() { Breed = mixed, BreedId = mixed.Id, Age = 4 },
                new Animal() { Breed = vizsla, BreedId = vizsla.Id, Age = 2 },
            };
            this.expectedAverages = new List<Averages>()
            {
                new Averages() { BreedName = "Mixed", AvgAge = 5 },
                new Averages() { BreedName = "Vizsla", AvgAge = 2 },
            };

            this.animalRepo.Setup(repo => repo.GetAll()).Returns(animals.AsQueryable());
            this.breedRepo.Setup(repo => repo.GetAll()).Returns(breeds.AsQueryable());
            return new ShelterLogic(this.animalRepo.Object, this.breedRepo.Object);
        }
    }
}
