﻿// <copyright file="ShelterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Shelter.Data.Models;
    using Shelter.Repository;

    /// <summary>
    /// Shelter logic, htat implements the IShelterLogic interfaace.
    /// </summary>
    public class ShelterLogic : IShelterLogic
    {
        private readonly IAnimalRepository animalRepo;
        private readonly IBreedRepository breedRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShelterLogic"/> class.
        /// </summary>
        /// <param name="animalRepo">Repository of the animals.</param>
        public ShelterLogic(IAnimalRepository animalRepo)
        {
            this.animalRepo = animalRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShelterLogic"/> class.
        /// </summary>
        /// <param name="animalRepo">Repository of the animals.</param>
        /// <param name="breedRepo">Repository of the breeds.</param>
        public ShelterLogic(IAnimalRepository animalRepo, IBreedRepository breedRepo)
        {
            this.animalRepo = animalRepo;
            this.breedRepo = breedRepo;
        }

        /// <inheritdoc/>
        public int AddBreed(string breedName)
        {
            if (this.breedRepo.GetAll().Where(x => x.Name == breedName).Any())
            {
                return 0;
            }

            return this.breedRepo.Add(breedName);
        }

        /// <inheritdoc/>
        public int AddAnimal(Animal animal)
        {
            if (animal == null)
            {
                throw new ArgumentNullException(nameof(animal));
            }

            if (string.IsNullOrEmpty(animal.Name))
            {
                throw new ArgumentException("Animals name cannot be empty");
            }

            if (animal.Age <= 0)
            {
                throw new ArgumentException("Animals age must be greater than zero");
            }

            return this.animalRepo.Add(animal);
        }

        /// <inheritdoc/>
        public IList<Breed> GetAllBreeds()
        {
            return this.breedRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Animal> GetAllAnimals()
        {
            return this.animalRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Animal> GetAnimalsByBreed(int breed)
        {
            return this.animalRepo.GetAll().Where(x => x.BreedId == breed).ToList();
        }

        /// <inheritdoc/>
        public Animal GetOneAnimal(int id)
        {
            return this.animalRepo.GetOne(id);
        }

        /// <summary>
        /// Removes the animal specified with the ID.
        /// </summary>
        /// <param name="id">ID of the animal, you want to remove.</param>
        /// <returns>Wether the removal was successful or not.</returns>
        public bool RemoveAnimal(int id)
        {
            return this.animalRepo.Remove(id);
        }

        /// <summary>
        /// Changes the animals age specified in the argument.
        /// </summary>
        /// <param name="id">ID of the animal.</param>
        /// <param name="newAge">New age of the animal.</param>
        public void ChangeAnimalAge(int id, int newAge)
        {
            this.animalRepo.ChangeAge(id, newAge);
        }

        /// <summary>
        /// Gets the average ages grouped by breed.
        /// </summary>
        /// <returns>List of the averages result.</returns>
        public IList<Averages> GetBreedAverageAge()
        {
            var res = from animal in this.animalRepo.GetAll()
                    group animal by new { animal.BreedId, animal.Breed.Name } into grp
                    select new Averages()
                    {
                        BreedName = grp.Key.Name,
                        AvgAge = grp.Average(x => x.Age),
                    };
            return res.ToList();
        }

        /// <summary>
        /// Gets the oldest animal.
        /// </summary>
        /// <returns>The oldest animal.</returns>
        public Animal GetOldestAnimal()
        {
            var res = from animal in this.animalRepo.GetAll()
                      orderby animal.Age descending
                    select animal;
            return res.FirstOrDefault();
        }
    }
}
