﻿// <copyright file="IShelterLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Logic
{
    using System.Collections.Generic;
    using Shelter.Data.Models;

    /// <summary>
    /// Shelter logic interface.
    /// </summary>
    public interface IShelterLogic
    {
        /// <summary>
        /// The animal that matches this ID.
        /// </summary>
        /// <param name="id">ID of the animal.</param>
        /// <returns>The only animal, that matches this ID.</returns>
        Animal GetOneAnimal(int id);

        /// <summary>
        /// Gets all the animals in the context.
        /// </summary>
        /// <returns>List of all the animals.</returns>
        IList<Animal> GetAllAnimals();

        /// <summary>
        /// Adds a new breed with the name passed in the argument.
        /// </summary>
        /// <param name="breedName">Name of the breed, you want to add.</param>
        /// <returns>Newly added breeds ID.</returns>
        int AddBreed(string breedName);

        /// <summary>
        /// Animal you want to add.
        /// </summary>
        /// <param name="animal">Animal to be added.</param>
        /// <returns>Id of the newly added animal.</returns>
        int AddAnimal(Animal animal);

        /// <summary>
        /// Gets all the breeds in the context.
        /// </summary>
        /// <returns>List of all the breeds.</returns>
        IList<Breed> GetAllBreeds();

        /// <summary>
        /// Gets all the animals in one specific breed.
        /// </summary>
        /// <param name="breedId">Id of the breed.</param>
        /// <returns>List of all the animal in the given breed.</returns>
        IList<Animal> GetAnimalsByBreed(int breedId);
    }
}
