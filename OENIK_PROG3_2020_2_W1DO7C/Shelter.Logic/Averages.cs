﻿// <copyright file="Averages.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Shelter.Logic
{
    /// <summary>
    /// Averages class. This is used to store the averages results.
    /// </summary>
    public class Averages
    {
        /// <summary>
        /// Gets or sets name of the breed.
        /// </summary>
        public string BreedName { get; set; }

        /// <summary>
        /// Gets or sets average age in the breed.
        /// </summary>
        public double AvgAge { get; set; }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns>A formatted string which contains all the relevant information about the calculated avereages reult.</returns>
        public override string ToString()
        {
            return $"Breed = {this.BreedName} - Avg = {this.AvgAge}";
        }

        /// <summary>
        /// Equals override.
        /// </summary>
        /// <param name="obj">Other object.</param>
        /// <returns>true if this and obj is equals. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Averages)
            {
                Averages other = obj as Averages;
                return this.BreedName == other.BreedName &&
                    this.AvgAge == other.AvgAge;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// GetHashCode override.
        /// </summary>
        /// <returns>Custom generated hash code.</returns>
        public override int GetHashCode()
        {
            return this.BreedName.GetHashCode() + (int)this.AvgAge;
        }
    }
}
